package es.uah.ssd;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeTokenRequest;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson.JacksonFactory;
import com.google.appengine.labs.repackaged.org.json.JSONException;
import com.google.appengine.labs.repackaged.org.json.JSONObject;



@SuppressWarnings("serial")


public class GetDatos extends HttpServlet{
	
	
	
	private String email;
	private String verificado;
	private String nombre;
	private String apellido;
	private String url_foto;
	private String genero;
	private String pais;
	private String code, error;
	
	public void service(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException{
		
	
		//Comprobamos si la autenticación ha sido correcta o no, por ello debemos ver si
		// viene el parámetro "code" o el parámetro "error"
		
		//Si todo bien, continuamos con el flujo de OAuth, si no, redirigimos a una página
		// de error
		
		Enumeration paramNames = request.getParameterNames();
		
		while(paramNames.hasMoreElements())
		{
		      String paramName = (String)paramNames.nextElement();
		      if(paramName.equals("error")){
		    	  error = request.getParameter("error");
		    	  if(error.equals("access_denied")){
		    		  response.sendRedirect("/error.html");
		    	  }
		      }
		       if(paramName.equals("code")){
			       code = request.getParameter("code");		    
		      }
		}
		
		

		HttpSession session=request.getSession(true);
	    if(session!=null){
			
			
			@SuppressWarnings("unused")
			
			HttpTransport HTTP_TRANSPORT = new NetHttpTransport();//used inside 
			
					
			//Preparamos la petición que haremos a Google con el código que nos ha mandado
			// y el resto de los datos necesarios (secreto, id y callback url
			//
			//Es necesario definir una aplicación en Google API Console que nos facilite
			// los datos para poder acceder.
			HttpTransport transporte=new com.google.api.client.http.javanet.NetHttpTransport();
			String CLIENT_ID="24568465964-de6a1hg4keall49jm14k8jl2plmdf3f8.apps.googleusercontent.com";
			String CLIENT_SECRET="WEelRa_eVvo3iNn7uCvk-ZZH";
			String authorizationCode=code;
			String CALLBACK_URL="http://ssdoauth.appspot.com/GetDatos";
			

			//Para pedir el token, necesitamos un objeto de tipo JacksonFactory
			JacksonFactory JSON_FACTORY = new JacksonFactory();


			//Solicitamos el token a Google.
			GoogleAuthorizationCodeTokenRequest tokenRequest=new GoogleAuthorizationCodeTokenRequest(transporte,JSON_FACTORY, CLIENT_ID, CLIENT_SECRET, authorizationCode, CALLBACK_URL);
			GoogleTokenResponse tokenResponse=tokenRequest.execute();

			String original_token=tokenResponse.getAccessToken();
			GenericUrl googleApiGetWay=new GenericUrl("https://www.googleapis.com/oauth2/v1/userinfo?access_token="+original_token);

			
			//Solicitamos la información a Google
			GoogleCredential cradential=new GoogleCredential();
			HttpRequestFactory userrequest = new NetHttpTransport().createRequestFactory(cradential);
			HttpRequest googlerequest = userrequest.buildGetRequest(googleApiGetWay);
			HttpResponse userDetailsUrl = googlerequest.execute();
			
			
			//Manejamos la información recibida de Google (monetamos un JSON con ella)
			StringBuilder sb = new StringBuilder();
			BufferedReader userDetails = new BufferedReader(new InputStreamReader(userDetailsUrl.getContent()));
			String str ="";
			
			 
			int n=0;
			while( (str = userDetails.readLine()) != null ){
				sb.append(str);
				n++;
			}  
			
			try {
				JSONObject jObj = new JSONObject(sb.toString());
					
				email = jObj.getString("email");
				verificado =jObj.getString("verified_email");;
				nombre=jObj.getString("name");
				apellido=jObj.getString("family_name");
				url_foto=jObj.getString("picture");
				genero=jObj.getString("gender");
				pais=jObj.getString("locale");
					
			} catch (JSONException e) {
				e.printStackTrace();
			}


			//Guardamos los datos en la sesión para poder pasarlos a un JSP
		    session.setAttribute("nombre", nombre);
		    session.setAttribute("apellido", apellido);
		    session.setAttribute("genero", genero);
		    session.setAttribute("email", email);
		    session.setAttribute("verificado", verificado);
		    session.setAttribute("pais", pais);
		    session.setAttribute("url_foto", url_foto);

		    //Redireccionamos al JSP que se encargará de mostrar los datos.
		    response.sendRedirect("/vista.jsp");
		    
		}
	}
}
