package es.uah.ssd;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.labs.repackaged.org.json.JSONException;
import com.google.appengine.labs.repackaged.org.json.JSONObject;


public class Servidor extends HttpServlet {

    private String clavevigenere = null;
     
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    //Las funciones de encripta y desencripta son básicamente las mismas, consisten
    //en aplicar Vigenere, solo que con una clave inversa.
    //Las funcoines están basadas en la versión JavaScript del siguiente sitio:
    // http://nayuki.eigenstate.org/page/vigenere-cipher-javascript
    //Muy útil para probar y coparar.
    
    private static String encripta(String plain, String key){
 
        int i,j;
        int c;
        char cha;
        
        String output="";
        
        char[] plainChars=plain.toCharArray();
        char[] keyChars=key.toCharArray();
        
        int temp=0;
        
        char clavefiltrada[] = new char[key.length()];
        
        for (i= 0; i< keyChars.length; i++){
            cha = keyChars[i];
            if(Character.isLetter(cha)) {
                temp=cha;
                clavefiltrada[i]  =  Character.forDigit(((temp - 65) % 32), 32);  
            }
        }
        
         for (i = 0, j = 0; i < plainChars.length; i++) {
            c = (int)plainChars[i];   
            if (Character.isUpperCase(c)){
                output += (char)((c - 65 + Character.getNumericValue(clavefiltrada[j%clavefiltrada.length])) % 26 + 65);
                j++;
            }else if (Character.isLowerCase(c)){
                output += (char)((c - 97 + Character.getNumericValue(clavefiltrada[j%clavefiltrada.length])) % 26 + 97);
                j++;
            } else {
                output += (char)c;
            }
        }
         
         
        return output;
    }
    
    private static String desencripta(String plain, String key){
 
        int i,j;
        int c;
        char cha;
        
        String output="";
        
        char[] plainChars=plain.toCharArray();
        char[] keyChars=key.toCharArray();
        
        int[] clavenumerica=new int[keyChars.length];
        
        int temp=0;
        
        char clavefiltrada[] = new char[key.length()];
        
        for (i= 0; i< keyChars.length; i++){
            cha = keyChars[i];
            if(Character.isLetter(cha)) {
                temp=cha;
                clavenumerica[i]  =  ((temp - 65) % 32);
            }
        }
        
        for (i = 0; i < clavefiltrada.length; i++) {
            clavefiltrada[i] = Character.forDigit((26 - clavenumerica[i] % 26),32);
        }
        

         for (i = 0, j = 0; i < plainChars.length; i++) {
            c = (int)plainChars[i];   
            if (Character.isUpperCase(c)){
                output += (char)((c - 65 + Character.getNumericValue(clavefiltrada[j%clavefiltrada.length])) % 26 + 65);
                j++;
            }else if (Character.isLowerCase(c)){
                output += (char)((c - 97 + Character.getNumericValue(clavefiltrada[j%clavefiltrada.length])) % 26 + 97);
                j++;
            } else {
                output += (char)c;
            }
        }
        return output;
    }
    
    //La función traduce adapta la clave numérica negociada con el cliente a
    //una clave formada por letras únicamente.
    
    private static String traduce(String numeros){

        char numerosArr[] = new char[numeros.length()];
        
        String numerostr = Integer.toString(Integer.parseInt(numeros),26);
        numerosArr = numerostr.toCharArray();

        char salida[] = new char[numerostr.length()];
        
        for (int i = 0; i < numerostr.length(); i++){
            if (Character.isLetter(numerosArr[i])) {
                salida[i] = numerosArr[i];
            }
            else{
                salida[i] = (char) (Character.getNumericValue(numerosArr[i]) + 65);
                
            }
        }
        
        return String.valueOf(salida);

    }
    
    @Override
    public void service( HttpServletRequest peticion, HttpServletResponse respuesta)
    throws ServletException, IOException
    {

        String str, mensaje, claro;
        String G,Z,n;
        int b,tmp;
        BigInteger clave;
        boolean negocia=true;

            
        StringBuilder sb = new StringBuilder();
        BufferedReader br = peticion.getReader();

        //Recibimos la petición del cliente, queda almacenada en un StringBuilder.
        
        while( (str = br.readLine()) != null ){
            sb.append(str);
        }    

        try { 
            //Guardamos la petición como un objeto JSON (ya que ha venido así del cliente)
            JSONObject jObj = new JSONObject(sb.toString()); 

                //Extraemos los valores necesarios para Diffie-Hellman.
                G=jObj.getString("G");
                Z=jObj.getString("Z");
                n=jObj.getString("n");
                negocia=jObj.getBoolean("negocia");
                mensaje=jObj.getString("mensaje");
                claro=jObj.getString("claro");
                
                
                
                if(negocia){    //Se trata de una petición de negociación de clave
                
                    b = (int) Math.floor((Math.random()*10)+1); 
                    clave =  BigInteger.valueOf((long)((Math.pow(Integer.parseInt(n),b))%Integer.parseInt(G)));
                    tmp= (int) (Math.pow(Integer.parseInt(Z), b) % Integer.parseInt(G));
                    jObj.put("n", tmp);       
                    clave = clave.add(BigInteger.valueOf(17));
                    clave = clave.pow(4);    
                    
                    //Una vez tenemos la clave numérica negociada (clave) la transformamos.
                    //para poder utilizar Vigenere (clavevigenere).
                    clavevigenere=traduce(String.valueOf(clave));
                    
                    //Esta línea tiene caracter informativo, para comprobar que las
                    //claves negociadas realmente coinciden.
                    System.out.println("Clave negociada: " + clavevigenere);
                }
                else{       //Se trata de un mensaje cifrado
                    
                    //Mostramos por el log del servidor el mensaje recibido desencriptado y el que está en claro
                    System.out.println("Mensaje cifrado recibido:  " + mensaje + " - Desencriptado: " + desencripta(mensaje,clavevigenere));
                    System.out.println("Mensaje en claro recibido: " + claro);
                    
                    //Escribimos en el JSON un mensaje cifrado para responder al cliente
                    jObj.put("mensaje", encripta("Todo correcto!",clavevigenere)); 
                    //Esta línea tiene caracter informativo, para comprobar el buen
                    //funcionamiento del programa.
                    System.out.println("Enviado: " + encripta("Todo correcto, cierro conexion.",clavevigenere) + " Clave: " + clavevigenere);
                }
                
                //Devolvemos una respuesta al cliente en formato JSON.
                respuesta.setContentType("application/json;charset=UTF-8");
                respuesta.setHeader("Cache-Control", "no-store");
                PrintWriter out = respuesta.getWriter();
                out.write(jObj.toString());
                out.flush();
                out.close();

        } catch (JSONException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }   

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}