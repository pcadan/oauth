package es.uah.ssd;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@SuppressWarnings("serial")
public class PetGoogle extends HttpServlet{
	public void doGet(HttpServletRequest request,HttpServletResponse response)throws IOException{
		HttpSession session=request.getSession(true);
		
		//Hacemos la petición a Google para que se identifique el usuario
		// y conceda permiso para acceder a sus datos básicos de cuenta y email.
		response.sendRedirect("https://accounts.google.com/o/oauth2/auth?" +
				"scope=https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile&" +
				"redirect_uri=http://ssdoauth.appspot.com/GetDatos&response_type=code&" +
				"client_id=24568465964-de6a1hg4keall49jm14k8jl2plmdf3f8.apps.googleusercontent.com");
		
	}

}
