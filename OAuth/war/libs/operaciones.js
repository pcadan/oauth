function traduce(numeros) {
    
    //Función utilizada para a partir del valor numérico negociado mediante
    //el algoritmo de Diffie-Hellman, formar una clave compuesta de letras
    //únicamente.
    //Para ello realizamos un cambio de base (a base 26)

    var salida = "";
    base26 = parseInt(numeros).toString(26);
    for (var i = 0; i < base26.length; i++){
        if (!isNaN(base26[i])){
            salida += String.fromCharCode(parseInt(base26[i])+65);
        }
        else{
            salida += base26[i];
        }
    }
    return salida.toString();
}    