// A partir del código de ejemplo de la siguiente web:
// http://nayuki.eigenstate.org/page/vigenere-cipher-javascript
// Muy útil para comprobar si los resultados son correctos.


//Las funciones de encriptar y desencriptar son básicamente la misma (aplicar
// Vigenere) pero modificando la clave que se aplicará (a su inversa).

function vigenere(input, key) {
    var output = "";
    
    for (var i = 0, j = 0; i < input.length; i++) {
        var c = input.charCodeAt(i);
    
        if (esMayuscula(c)) {
            output += String.fromCharCode((c - 65 + key[j % key.length]) % 26 + 65);
            j++;
        } else if (esMinuscula(c)) {
            output += String.fromCharCode((c - 97 + key[j % key.length]) % 26 + 97);
            j++;
        } else {
            output += input.charAt(i);
        }
    }
    return output;
}

function desencripta(texto,clave) {
    
    if (clave.length == 0) {
        alert("Clave en blanco!");
        return;
    }
    
    var key = filtraclave(clave);
        
    if (key.length == 0) {
        alert("Clave sin letras!");
        return;
    }
        
    for (var i = 0; i < key.length; i++){
        key[i] = (26 - key[i]) % 26;
    }

  
    return vigenere(texto, key);
}
function encripta(texto,clave){
    var key = filtraclave(clave);    
    return vigenere(texto, key);
}

//filtraclave lo que realiza es que a cada letra le asigna su posición en el alfabeto:
// a->0, b->1, c->2, d->3 ... z->25

function filtraclave(key) {
    var resulta = [];
    
    for (var i = 0; i < key.length; i++) {
        var c = key.charCodeAt(i);
        if (esLetra(c))
            resulta.push((c - 65) % 32);
    }
    return resulta;
}

function esLetra(c) {
        return esMayuscula(c) || esMinuscula(c);
}

function esMayuscula(c) {
        return c >= 65 && c <= 90;  
}

function esMinuscula(c) {
        return c >= 97 && c <= 122;  
}