<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<HEAD>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <script src="libs/jquery.js" type="text/javascript"></script>
        <script src="libs/bignumber.js" type="text/javascript"></script>
        <script src="libs/vigenere.js" type="text/javascript"></script>
        <script src="libs/operaciones.js" type="text/javascript"></script>
        
        
        <SCRIPT type="text/javascript">
            
            // clave es el número acordado con el servidor utilizando Diffie-Hellman
            var clave;
            // clvevigenere es una clave recalculada a partir de la anterior para poder
            // ser utilizada con el algoritmo de Vigenere
            var clavevigenere;

            // En el evento load de la página negociamos una nueva clave utilizando
            // Diffie-Hellman.
            // Una vez que tengamos una clave en común, podremos utilizarla para
            // cifrar la comunicación utilizando Vigenere.
            
            $(window).load(function() {
                var a = Math.floor((Math.random()*13)+1); 
                var G = Math.floor((Math.random()*100)+1); 
                var Z = Math.floor((Math.random()*17)+1); 
                //Necesitamos utilizar la librería BigNumber para poder manejar
                //enteros grandes.
                var n = new BigNumber("123456789012345678901234567890123456789");
                
                n = Math.pow(Z,a)%G;

                //Utilizamos AJAX para no recargar nuevamente la página y para
                // mandar/recibir objetos JSON
                $.ajax({
                    type: 'post',
                    url: 'http://ssdoauth.appspot.com/Servidor',
                    dataType: 'json',
                    //Mandamos el campo negocia=true para que el servidor no descifre,
                    // y simplemente negocie la clave
                    data: JSON.stringify({ "G" : G.toString(), "Z": Z.toString(), "n": n.toString(), "negocia" : true, "mensaje": "","claro":"" }),
                    contentType: 'application/json; charset=utf-8',
                    success: function(datos) {
                        clave=  Math.pow((Math.pow(datos.n,a)%G)+17,4);
                        clavevigenere=traduce(clave.toString());
                        //la siguiente línea tiene propósitos informativos, para
                        //comprobar que realmente se negocia una clave común.
                        $("#respuesta").html("Clave negociada: " + clavevigenere);
                    }
                });
            });
            
            
            
            //Al pulsar el botón mandamos al servidor el texto introducido, dicho texto
            //estará cifrado utilizando la clave común y el algoritmo de Vigenere.
            
            $(document).ready(function(){
                
                $("#enviar").click(function()
                {
                    //encriptamos el mensaje y lo guardamos.
                    var mensajecifrado = encripta($("#mensaje").val(),clavevigenere);
                    var mensajeclaro = $("#claro").val()
                    
                    $.ajax({
                    type: 'post',
                    url: 'http://ssdoauth.appspot.com/Servidor',
                    dataType: 'json',
                    //Mandamos el campo negocia=false y el mensaje para que el servidor lo descifre,
                    // ya que la clave ya está negociada anteriormente (en el load)
                    data: JSON.stringify({ "G" : "G", "Z" : "Z", "n" : "n", "negocia" : false, "mensaje": mensajecifrado, "claro":mensajeclaro}),
                    contentType: 'application/json; charset=utf-8',
                    success: function(datosR) {
                        //Esta línea es para comprobar que en el cliente también podemos desencriptar
                        //un mensaje que llegue cifrado del servidor.
                        $("#respuesta").html("Mensaje cifrado recibido: " + datosR.mensaje + " ---> " + desencripta(datosR.mensaje,clavevigenere));

                        }
                    });
                });
            });
            
            
            
        </SCRIPT>
    </HEAD>
  
  <body>     
  
  			<h1>Práctica OAuth - UAH</h1>
			<h2>Seguridad en sistemas distribuidos - 2013</h2>
			<h2>Pedro Carlos Adán Palacios</h2>
			<p></p>
			
	        <%

	        	if(session.getAttribute("verificado")==null){

	        		
	        		out.println("<p>ATENCIÓN!! No estás autentificado en Google.</p>");
	        		out.println("<p></p>");
	        		out.println("<p>Si deseas utilizar la aplicación inicia sesión y autoriza la aplicación.</p>");
	        		out.println("<a href='http://ssdoauth.appspot.com'><img src='imagenes/inicio.jpg' width='94' height='64'></a>");
	        		
	        		
	        	}else{
	        		out.println("<p>Identificación realizada en Google correctamente.</p>");
	        		
	        		out.println("<p> <img src=" + session.getAttribute("url_foto") + "></p>");	
	        		out.println("<p>Nombre: " + session.getAttribute("nombre") +"</P>");
	        		out.println("<p>Apellido: " + session.getAttribute("apellido") + "</P>");
	        		out.println("<p>eMail: " + session.getAttribute("email") + "</P>");
	        		
	        		out.println("<form name='envio'>");
	        		out.println("<br><label>Introduce Texto para enviar cifrado: </label>");
	        		out.println("<br><input type='text' size='60' class='txt' id='mensaje' />");
	        		out.println("<br><label>Introduce Texto para enviar en claro </label>");		
	        		out.println("<br><input type='text' size='60' class='txt' id='claro' />	");
	        		out.println("<br><input type='button' id='enviar' value='enviar' />");
	        		out.println("<br>El mensaje recibido es:");
	        		out.println("<div id='respuesta' style='color:green'></div>");
	        		out.println("</form>");


	        	}
			%>

  </body>
  
</html>